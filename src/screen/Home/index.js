import React, { Component } from "react";
import { View, Text } from "react-native";
import {
  Container,
  Header,
  Body,
  Form,
  Item,
  Label,
  Input,
  Button,
  Icon
} from "native-base";
import { connect } from "react-redux";
import { addTodo } from "../../redux/action/todoAction";

class TodoApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todo: ""
    };
  }

  componentDidMount() {
    console.log("my props", this.props);
  }
  handleChangeTodoText = todo => {
    this.setState({
      todo
    });
  };

  handleAddTodo = () => {
    this.props.addTodo(this.state.todo);
  };
  render() {
    let { todo } = this.state;
    let { todoList } = this.props;
    return (
      <Container>
        <Header>
          <Body>
            <Text>Todo App</Text>
          </Body>
        </Header>

        <Form>
          <Item inlineLabel>
            <Label>Todo</Label>
            <Input value={todo} onChangeText={this.handleChangeTodoText} />
          </Item>
        </Form>

        <View
          style={{ width: "50%", alignSelf: "center", paddingVertical: 20 }}
        >
          <Button
            bordered
            style={{
              width: "100%",
              paddingHorizontal: 10,
              justifyContent: "center"
            }}
            onPress={this.handleAddTodo}
          >
            <Text>Add Todo</Text>
          </Button>
        </View>

        <View>
          {todoList.map((data, index) => {
            return <Text key={index}>{data}</Text>;
          })}
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    todoList: state.todo
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addTodo: todo => dispatch(addTodo(todo))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoApp);

import { ADD_TODO } from "../action/todoAction";
import { combineReducers } from "redux";
const initailState = {
  test: {},
  todo: []
};

const todoReducer = (state = initailState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todo: [...state.todo, action.payload]
      };
    default:
      return state;
  }
};

export { todoReducer };

import { createStore, applyMiddleware } from "redux";
import { todoReducer } from "./reducer";
import logger from "redux-logger";

const store = createStore(todoReducer, applyMiddleware(logger));
export { store };
